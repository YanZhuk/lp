var lpApp = angular.module('lpApp', []);

lpApp.controller('lpPriceCtrl', ['$scope', '$http', function ($scope, $http) {

    $http.get('price.json').then(function (response) {
        $scope.prices = response.data;
        $scope.calc();
        $scope.sortGet();
    });

    $scope.calc = function () {
        $scope.prices.forEach(function (price, i) {
            $scope.prices[i].sum = $scope.prices[i].price * (1 - $scope.prices[i].discount);
        });
    }

    $scope.sortSet = function (propertyName) {
        if ($scope.sortBy == propertyName) $scope.sortRev = !$scope.sortRev;
        $scope.sortBy = propertyName;
        localStorage.sortBy = $scope.sortBy;
        localStorage.sortRev = $scope.sortRev;
    }

    console.log(localStorage.sortBy);
    console.log(localStorage.sortRev);

    $scope.sortGet = function () {
        if (localStorage.sortBy && localStorage.sortRev) {
            $scope.sortBy = localStorage.sortBy;
            $scope.sortRev = localStorage.sortRev == 'true';
        } else {
            $scope.sortBy = 'name';
            $scope.sortRev = false;
        }
    }

}]);

(function ($) {
    $(document).ready(function () {

       
        function lpHeader() {
            if ($(window).scrollTop() == 0) { 
                $('header').addClass('top'); 
            } else { 
                $('header.top').removeClass('top');
            }
        }
        
        lpHeader(); 
        $(window).on('scroll', lpHeader); 

        
        var lpMenuItems = $('ul.nav li a');
        
        lpMenuItems.each(function () {
            
            var link = $(this),
                linkHref = link.attr('href'),
                linkTrgt = $(linkHref); 
            
            if (linkTrgt.length > 0) {
                
                link.on('click', function (e) {
                    
                    e.preventDefault();
                   
                    var offset = linkTrgt.offset().top;
                    
                    $('body, html').animate({
                        scrollTop: offset - 40
                    }, 750);
                });
            }
        });

       
        var lpNavItems = [];
       
        function lpSetNavItems() {
            
            lpNavItems = [];
            
            $('section').each(function () {
                lpNavItems.push({
                    top: $(this).offset().top, 
                    name: $(this).attr('id') 
                });
            });
        }
        
        lpSetNavItems();
        $(window).on('resize', lpSetNavItems);

        
        var lpPath = location.pathname.replace('/', '');

        function lpGoToActive() {
            lpNavItems.forEach(function (item) {
                if (item.name == lpPath) {
                    setTimeout(function () {
                        $('body, html').scrollTop(item.top - 40);
                    }, 500);
                }
            });
        }
        
        lpGoToActive(); 
        $(window).on('load', lpGoToActive); 

        
        function lpSetNavActive() {
            
            var curItem = '';
            
            lpNavItems.forEach(function (item) {
                
                if ($(window).scrollTop() > item.top - 200) {
                    curItem = item.name; 
                }
            });
           
            if ($('ul.nav li.active a').attr('href') != '#' + curItem || $('ul.nav li.active').length == 0) {
                
                $('ul.nav li.active').removeClass('active');
                
                $('ul.nav li a[href="#' + curItem + '"]').parent().addClass('active');
                
                history.pushState({
                    curItemName: curItem
                }, curItem, '/' + curItem);
            }
        }
       
        lpSetNavActive(); 
        $(window).on('scroll', lpSetNavActive); 

        

        $('.lp-slider1').owlCarousel({
            items: 1,
            nav: true,
            navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa fa-arrow-right"></i>'],
            animateOut: 'fadeOut'
        });

        $('.lp-slider2').owlCarousel({
            items: 4,
            nav: true,
            navText: ['<i class="fa fa-arrow-left"></i>', '<i class="fa fa-arrow-right"></i>']
        });

       
        $('.lp-tabs').each(function () {
            
            var tabs = $(this),
                tabsTitlesNames = []; 
            
            tabs.find('div[data-tab-title]').each(function () {
                tabsTitlesNames.push($(this).attr('data-tab-title'));
            }).addClass('lp-tab');
           
            tabs.wrapInner('<div class="lp-tabs-content"></div>');
            
            tabs.prepend('<div class="lp-tabs-titles"><ul></ul></div>');
           
            var tabsTitles = tabs.find('.lp-tabs-titles'),
                tabsContent = tabs.find('.lp-tabs-content'), 
                tabsContentTabs = tabsContent.find('div[data-tab-title]'); 
           
            tabsTitlesNames.forEach(function (value) {
                tabsTitles.find('ul').append('<li>' + value + '</li>');
            });
            
            var tabsTitlesItems = tabsTitles.find('ul li');
            
            tabsTitlesItems.eq(0).addClass('active');
            
            tabsContentTabs.eq(0).addClass('active').show();
            
            tabsContent.height(tabsContent.find('.active').outerHeight());
            
            tabsTitlesItems.on('click', function () {
               
                if (!tabs.hasClass('changing')) {
                    
                    tabs.addClass('changing');
                    
                    var curTab = tabsContent.find('.active'),
                        nextTab = tabsContentTabs.eq($(this).index()); 
                    
                    tabsTitlesItems.removeClass('active');
                    
                    $(this).addClass('active');
                    
                    var curHeight = curTab.outerHeight();
                    
                    nextTab.show();
                    
                    var nextHeight = nextTab.outerHeight();
                    
                    nextTab.hide();
                   
                    if (curHeight < nextHeight) {
                        
                        tabsContent.animate({
                            height: nextHeight
                        }, 500);
                    }
                    
                    curTab.fadeOut(500, function () {
                        
                        if (curHeight > nextHeight) {
                            
                            tabsContent.animate({
                                height: nextHeight
                            }, 500);
                        }
                       
                        nextTab.fadeIn(500, function () {
                           
                            curTab.removeClass('active');
                            
                            nextTab.addClass('active');
                            
                            tabs.removeClass('changing');
                        });
                    });

                }
            });
            
            $(window).on('resize', function () {
               
                tabsContent.height(tabsContent.find('.active').outerHeight());
            });
        });

        

        $('.lp-mfp-inline').magnificPopup({
            type: 'inline'
        });

        $('.lp-gallery').each(function () {
            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        });

        

        $('#lp-fb1').wiFeedBack({
            fbScript: 'blocks/wi-feedback.php',
            fbLink: false,
            fbColor: '#6f11a8'
        });

        $('#lp-fb2').wiFeedBack({
            fbScript: 'blocks/wi-feedback.php',
            fbLink: '.lp-fb2-link',
            fbColor: '#6f11a8'
        });



    });
})(jQuery);
        
        function lpMapInit(){
            var lpMapOptions={
                center: [53.906388333333325, 27.51017888888888],
                zoom: 13,
                controls: ['zoomControl'],
                behaviors:  ['drag']
            }
            var lpMap=new ymaps.Map("lp-map", lpMapOptions);
            lpPlacemark = new ymaps.Placemark([53.906388333333325, 27.51017888888888]);
            lpMap.geoObjects.add(lpPlacemark);
        }

     